from flask import Flask, render_template, request
from flask_socketio import SocketIO, send, emit

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secretkey'
app.config['DEBUG'] = True
socketio = SocketIO(app)

users = {}

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/originate')
def orginate():
    socketio.emit('server_orginated', 'Something happened on the server!')
    return '<h1>Sent!</h1>'

#Save user session id.
@socketio.on('username', namespace='/private')
def receive_username(username):
    users[username] = request.sid
    # users.append({username : request.sid})
    # print(users)
    print('Username added!')

#Send private message based on user session id.
@socketio.on('private_message', namespace='/private')
def private_message(payload):
    recipient_session_id = users[payload['username']]
    message = payload['message']
    emit('new_private_message', message, room=recipient_session_id)

# @socketio.on('message from user', namespace='/messages')
# def receive_message_from_user(message):
#     print('USER MESSAGE: {}'.format(message))
#     emit('from flask', message.upper(), broadcast=True)



if __name__ == '__main__':
    socketio.run(app)